Categories:Writing,Reading,Science & Education
License:MIT
Web Site:https://github.com/Anthony-Reboul/AndroidDeepL
Source Code:https://github.com/Anthony-Reboul/AndroidDeepL
Issue Tracker:https://github.com/Anthony-Reboul/AndroidDeepL/issues
Changelog:https://github.com/Anthony-Reboul/AndroidDeepL/releases
Bitcoin:3DetrDfft3ChjXMQUsTwQL1ozQvBXwGSsQ
Litecoin:LXRwRuKMpqSepWobvpKaCKYFHrtwQC4bqM

Summary:Unofficial application of DeepL Translator
Description:
OpenL Translator is an open source version of the DeepL services.

In blind tests pitting DeepL Translator against the competition, translators
prefer our results by a factor of 3:1. Available languages: English, German,
French, Spanish, Italian, Dutch and Polish.
.

Repo Type:git
Repo:https://github.com/Anthony-Reboul/AndroidDeepL

Build:2.0.1,5
    commit=2.0.1
    subdir=app
    gradle=openlWithoutPlayServices
    prebuild=sed -n -i -e '/maven.fabric/{s/.*//;x;N;d;};x;p;${x;p;}' ../build.gradle && \
        sed -i -e '/io.fabric/d' ../build.gradle build.gradle && \
        sed -i -e '/google-services/d' ../build.gradle build.gradle && \
        sed -i -e '/withPlayServicesImplementation(.*) {/,/}/d; /withPlayServicesImplementation/d' build.gradle

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:2.0.1
Current Version Code:5
